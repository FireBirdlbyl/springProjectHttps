package j1017.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import j1017.model.Material;
import j1017.model.Robot;

public interface RobotJPARepository extends JpaRepository<Robot, Long> {
	
	@Query("from Robot where material=:material")
	public Set<Robot> findByMaterial(@Param("material")Material material);
}

package dao;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import j1017.model.Robot;

@Transactional
@Repository
public class RobotDaoImpl implements IRobotDao {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Robot> getAllRobots() {
		String hql = "FROM robots as rb ORDER BY rb.id";
		
		return (List<Robot>)entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Robot getRobotById(Long robotId) {
		return entityManager.find(Robot.class, robotId);
	}

	@Override
	public void addRobot(Robot robot) {
		entityManager.persist(robot);

	}

	@Override
	public void updateRobot(Robot robot) {
		Robot robotl = getRobotById(robot.getId());
		robotl.setName(robot.getName());
		robotl.setCapacity(robot.getCapacity());
		robotl.setDatecreate(robot.getDatecreate());
		entityManager.flush();
	}

	@Override
	public void deleteRobot(Long robotId) {
		entityManager.remove(getRobotById(robotId));
	}

}
